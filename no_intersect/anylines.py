import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import time

# point class   
class Point:
    def __init__(self,x,y,side):
        self.x = x
        self.y = y
        self.side = side # 0 is left. 1 is right.
        self.segment = None


# segment class
class Segment:
    def __init__(self, left,right):
        self.left = left
        self.right = right

#------------------------------------------------------------------------------------------
# Generate some segments:
def GenerateSegments(n=10, h =100, show = 1):
    # Returns an array of n segments and an array of 2n end-points from each segment
    segs = []
    points = []

    for i in range(n):
        v = np.zeros(4)
        #check for vertical points:
        while(v[0] == v[2]): #x1 == x2 means vertical point, so generate a new one.
            v = np.random.randint(low=1, high=h, size=4)
        
        left = Point(min(v[0],v[2]), v[1], 0)
        right = Point(max(v[0],v[2]), v[3], 1)

        s1 = Segment(left,right)
        left.segment =s1
        right.segment=s1
        #add segment to list of segments:
        segs.append(s1)
        # add segments left and right points to list of points:
        points.append(left)
        points.append(right)
    if(show>0):
        for s in segs:
            X = [s.left.x,s.right.x]
            Y =[s.left.y,s.right.y]

            plt.plot(X,Y)
        plt.show()
    return segs, points

#------------------------------------------------------------------------------------------

# Merge Sort (Modified from Cormen):


def Merge(A,p,q,r):
    n1 = q-p+1
    n2 = r-q
    L = []                               # (MAX: n1+1 elements)
    R = []                               # (MAX: n2+1 elements)
    for i in range(n1): # i=1:n1
        L.append(A[p+i])
        
    for j in range(n2): # i=1:n2
        R.append(A[q+j+1])
    
    # Sentinel points:
    #n1+1
    L.append(Point(np.inf,np.inf,0)) 
    #n2+1
    R.append(Point(np.inf,np.inf,0)) 
    
    i = 0
    j = 0
    for k in range(p,r+1): # k=p:r
        if(L[i].x == R[j].x and L[i].side == R[j].side): 
            if(L[i].y < R[j].y):                 # case 3
                A[k] = L[i]                      # case 3
                i = i+1                          # case 3
            else:                                # case 3
                A[k] = R[j]                      # case 3
                j = j+1                          # case 3

        elif(L[i].x == R[j].x):
            if(L[i].side < R[j].side):           # case 2 sort by left side first
                A[k] = L[i]                      # case 2
                i = i+1                          # case 2
            else:                                # case 2
                A[k] = R[j]                      # case 2
                j = j+1                          # case 2
        
        elif(L[i].x < R[j].x):
            A[k] = L[i]                          # case 1
            i = i+1                              # case 1
        else:                                    # case 1
            A[k] = R[j]                          # case 1
            j = j+1                              # case 1

            
def MergeSort(A,p,r):
    if(p<r):
        q = int((p+r)/2)
        MergeSort(A,p,q)
        MergeSort(A,q+1,r)
        Merge(A,p,q,r)

#------------------------------------------------------------------------------------------

# Cross Product:
def Cross_Product(p1,p2):
    c = (p1.x*p2.y) - (p2.x*p1.y)
    return c

#===========================================================================================

#  Modified Node Struct


# Each node of the tree now contains the attributes 
# color, 
# segment, 
# left, 
# right, 
# and p. 

# If a child or the parent of a node does not exist, the corresponding pointer attribute
# of the node contains the value NIL . We shall regard these NIL s as being pointers to
# leaves (external nodes) of the binary search tree and the normal, key-bearing nodes
# as being internal nodes of the tree. (Cormen)

class Node:
    def __init__(self, segment, colour="red", left=None, right=None, p=None):
        self.colour = colour
        self.segment = segment
        if(not segment):
            self.key = None
        else:
            self.key = Cross_Product(segment.left, segment.right)
        self.left = left
        self.right = right
        self.p = p
#------------------------------------------------------------------------------------------       


# Modified Tree struct (Cormen):

class RBTree:
    def __init__(self):
        self.nil = Node(None,"black") 
#         self.root =Node(root_key, "black",self.nil,self.nil,self.nil)
        # Must be able to initialiase an empty tree
        self.root = self.nil
#     -----------------------------------------------------------
#     -----------------------------------------------------------
    
    
    
    # Rotation Algorithms:
#     -----------------------------------------------------------
    # Left_rotate(Tree, node):
    def Left_Rotate(self, x):
#         print("left rotate:", x.key)
        y = x.right               # set y
        x.right = y.left          # turn  y's left subtree into x's right subtree.
        if(y.left != self.nil):
            y.left.p = x
        y.p = x.p                 # link x's parent to y
        if(x.p == self.nil):
            self.root = y
        elif(x == x.p.left):
            x.p.left = y
        else:
            x.p.right = y
        y.left = x               # put x on y's left
        x.p = y
        
#     -----------------------------------------------------------

        # Right Rotate(Tree, node) (symmetric to Left Rotate)
    def Right_Rotate(self,x):
#         print("right rotate:", x.key)
        y = x.left               # set y
        x.left = y.right          # turn  y's right subtree into x's left subtree.
        if(y.right != self.nil):
            y.right.p = x
        y.p = x.p                 # link x's parent to y
        if(x.p == self.nil):
            self.root = y
        elif(x == x.p.right):
            x.p.right = y
        else:
            x.p.left = y
        y.right = x               # put x on y's right
        x.p = y
    
    
#     -----------------------------------------------------------
#     -----------------------------------------------------------
    
    #Insertion Algorithms
    
    def RB_Insert_Fix(self, new_node):
        while new_node.p.colour == "red":
            if(new_node.p == new_node.p.p.left):
                y = new_node.p.p.right
                if(y.colour=="red"):
                    new_node.p.colour = "black"          # case 1
                    y.colour = "black"                   # case 1
                    new_node.p.p.colour = "red"          # case 1
                    new_node = new_node.p.p              # case 1
                elif(new_node == new_node.p.right):
                    new_node = new_node.p                # case 2
                    self.Left_Rotate(new_node)           # case 2
                else:
                    new_node.p.colour = "black"          # case 3
                    new_node.p.p.colour = "red"          # case 3
                    self.Right_Rotate(new_node.p.p)      # case 3
            else: # same then clause but with 'left' and 'right' exchanged
                y = new_node.p.p.left
                if(y.colour=="red"):
                    new_node.p.colour = "black"          # case 1
                    y.colour = "black"                   # case 1
                    new_node.p.p.colour = "red"          # case 1
                    new_node = new_node.p.p              # case 1
                elif(new_node == new_node.p.left):
                    new_node = new_node.p                # case 2
                    self.Right_Rotate(new_node)          # case 2
                else:
                    new_node.p.colour = "black"          # case 3
                    new_node.p.p.colour = "red"          # case 3
                    self.Left_Rotate(new_node.p.p)       # case 3

        self.root.colour = "black"

 #     -----------------------------------------------------------


    def insert(self, new_node):
        y = self.nil
        x = self.root
        
        while(x != self.nil ):
            y = x
            if(new_node.key < x.key):
                x = x.left
            else:
                x = x.right
        new_node.p = y
        if(y == self.nil):
            self.root = new_node
        elif(new_node.key < y.key):
            y.left = new_node
        else:
            y.right = new_node
        
        # set leaves:
        new_node.left = self.nil
        new_node.right = self.nil
        # assign default colour before fixup:
        new_node.colour = "red"
#         print("root before fix", self.root.key)
        self.RB_Insert_Fix(new_node)
        

        
#     -----------------------------------------------------------
#     -----------------------------------------------------------

    # Deletion Algorithms (Cormen)
    
    # RB Transplant
    def RB_Transplant(self, u, v):
        if(u.p == self.nil):
            self.root = v
        elif(u == u.p.left):
            u.p.left = v
        else:
            u.p.right = v
        v.p = u.p

#     -----------------------------------------------------------

        
#     RB_Delete_Fix
    def RB_Delete_Fix(self, x):
        while(x != self.root and x.colour == "black"):
            if(x == x.p.left):
                w = x.p.right
                if(w.colour == "red"):
                    w.colour = "black"       # case 1
                    x.p.colour = "red"       # case 1
                    self.Left_Rotate(x.p)    # case 1
                    w = x.p.right            # case 1
                if(w.left.colour == "black" and w.right.colour == "black"):
                    w.colour = "red"         # case 2
                    x=x.p                    # case 2
                elif(w.right.colour == "black"):
                    w.left.colour = "black"  # case 3
                    w.colour = "red"         # case 3
                    self.Right_Rotate(w)     # case 3
                    w = x.p.right            # case 3
                else:
                    w.colour = x.p.colour        # case 4
                    x.p.colour = "black"         # case 4
                    w.right.colour = "black"     # case 4
                    self.Left_Rotate(x.p)        # case 4
                    x = self.root                # case 4
                
            else: # same as then clause with 'left' and 'right' exchanged
                w = x.p.left
                if(w.colour == "red"):
                    w.colour = "black"         # case 1
                    x.p.colour = "red"         # case 1
                    self.Right_Rotate(x.p)     # case 1
                    w = x.p.left               # case 1
                if(w.right.colour == "black" and w.left.colour == "black"):
                    w.colour = "red"           # case 2
                    x = x.p                    # case 2
                elif(w.left.colour == "black"):
                    w.right.colour = "black"   # case 3
                    w.colour = "red"           # case 3
                    self.Left_Rotate(w)        # case 3
                    w = x.p.left               # case 3
                else:
                    w.colour = x.p.colour          # case 4
                    x.p.colour = "black"           # case 4
                    w.left.colour = "black"        # case 4
                    self.Right_Rotate(x.p)         # case 4
                    x = self.root                  # case 4
        #end while
        x.colour = "black"
        
#     -----------------------------------------------------------
                    
    # Tree Minimum(x)
    def Tree_Min(self,x):
        while(x.left != self.nil):
            x = x.left
        return x
                    
                        
    # Tree Max(x)
    def Tree_Max(self,x):
        while(x.right != self.nil):
            x = x.right
        return x     
    
#     -----------------------------------------------------------
    
    # RB Delete
    def RB_Delete(self, condemned):
        y = condemned
        y_old_colour = y.colour
        if(condemned.left == self.nil):
            x = condemned.right
            self.RB_Transplant(condemned, condemned.right)
        elif(condemned.right == self.nil):
            x = condemned.left
            self.RB_Transplant(condemned, condemned.left)
        else:
            y = self.Tree_Min(condemned.right)
            y_old_colour = y.colour
            x = y.right
            if(y.p == condemned):
                x.p = y
            else:
                self.RB_Transplant(y, y.right)
                y.right = condemned.right
                y.right.p = y
            self.RB_Transplant(condemned, y)
            y.left = condemned.left
            y.left.p = y
            y.colour = condemned.colour
        if(y_old_colour == "black"):
            self.RB_Delete_Fix(x)


        
#     -----------------------------------------------------------
#     -----------------------------------------------------------


    # In-Order Tree Walk(node):
    # (Cormen)

    def Inorder_Tree_Walk(self, node):
        if(node != self.nil):
            self.Inorder_Tree_Walk(node.left)
            if(node == self.root):
                print(node.key,node.colour,"root")
            else:
                print(node.key,node.colour,"->",node.p.key)
            self.Inorder_Tree_Walk(node.right)

#     -----------------------------------------------------------


    # Search for node containing query segment and key (cross prod)
    # (Cormen)
    def Tree_Search(self, key, x=None):
        if(not x):
            return self.Tree_Search(key, self.root)
        if(x == self.nil or key == x.key):
            return x
        if(key < x.key):
            return self.Tree_Search(key, x.left)
        else:
            return self.Tree_Search(key, x.right)
        
    #----------------------------------------------------------------
    
    # Tree Predecessor (Cormen)
    
    def Above(self, node):
        if(node.left != self.nil):
            return self.Tree_Max(node.left)
        y = node.p
        while(y != self.nil and node == y.left):
            node = y
            y = y.p
        return y


    #----------------

    # Tree Successor (Cormen)

    def Below(self, node):
        if(node.right != self.nil):
            return self.Tree_Min(node.right)
        y = node.p
        while(y != self.nil and node == y.right):
            node = y
            y = y.p
        return y

    #----------------------------------------------------------------

#=====================================================================


# Any-Segments-Intersect (Cormen)


# Direction subroutine   
def Direction(pi,pj,pk):
    p1x = pk.x-pi.x
    p1y = pk.y-pi.y
    p1 = Point(p1x,p1y,0)            #side is arbitrary in this case, so just use left to make constructor happy.
    
    p2x = pj.x-pi.x
    p2y = pj.y-pi.y
    p2 = Point(p2x,p2y,0)
    
    return Cross_Product(p1,p2)    

#----------------------------------

def On_Segment(pi,pj,pk):
    if( (min(pi.x, pj.x) <= pk.x and pk.x <= max(pi.x, pj.x)) and 
       (min(pi.y,pj.y) <= pk.y and pk.y <= max(pi.y,pj.y)) ):
        return True
    else:
        return False



#----------------------------------

def Intersects(Seg1, Seg2):
    p1 = Seg1.left
    p2 = Seg1.right
    
    p3 = Seg2.left
    p4 = Seg2.right
    
    d1 = Direction(p3,p4,p1)
    d2 = Direction(p3,p4,p2)
    d3 = Direction(p1,p2,p3)
    d4 = Direction(p1,p2,p4)
    
    if( ( (d1>0 and d2<0) or (d1<0 and d2>0) ) and ( (d3>0 and d4<0) or (d3<0 and d4>0) ) ):
        return True
    elif(d1 == 0 and On_Segment(p3,p4,p1)):
        return True
    elif(d2 == 0 and On_Segment(p3,p4,p2)):
        return True
    elif(d3 == 0 and On_Segment(p1,p2,p3)):
        return True
    elif(d4 == 0 and On_Segment(p1,p2,p4)):
        return True
    else:
        return False
    
#----------------------------------
   
    
def Any_Segments_Intersect(S,P):
    T = RBTree()
    MergeSort(P,0,len(P)-1)                # sort the endpoints of the segments in S...
    for p in P:
        if(p.side == 0):                   # p is left endpoint of a segment
            s = Node(p.segment)
            T.insert(s)
            if( (T.Above(s) != T.nil and Intersects(T.Above(s).segment, s.segment)) or 
               (T.Below(s) != T.nil and Intersects(T.Below(s).segment, s.segment)) ):
                return True
        else:                            # p is right endpoint of a segment
            key = Cross_Product(p.segment.left, p.segment.right)
            s = T.Tree_Search(key)
            if( (T.Above(s) != T.nil and T.Below(s) != T.nil) and 
               (Intersects(T.Above(s).segment, T.Below(s).segment) ) ):
                return True
            T.RB_Delete(s)
    return False

#=============================================================================================================
#=============================================================================================================

# Experiments


# ---------------------------------------------------------------------------


# Generate segments with no intersections:

def GenerateSegments2(n=10, h =100, show = 1):
    # Returns an array of n segments and an array of 2n end-points from each segment
    segs = []
    points = []

    for i in range(n):
        v = np.zeros(4)
        #check for vertical points:
        while(v[0] == v[1]): #x1 == x2 means vertical point, so generate a new one.
            v = np.random.randint(0,10000,2)
            c = np.random.randint(0+i*10,10+10*i,2)
        
        left = Point(min(v[0],v[1]),c[0], 0)
        right = Point(max(v[0],v[1]),c[1], 1)

        s1 = Segment(left,right)
        left.segment =s1
        right.segment=s1
        #add segment to list of segments:
        segs.append(s1)
        # add segments left and right points to list of points:
        points.append(left)
        points.append(right)
    if(show>0):
        for s in segs:
            X = [s.left.x,s.right.x]
            Y =[s.left.y,s.right.y]

            plt.plot(X,Y)
        plt.savefig(str(n)+'_segments.pdf', format='pdf', bbox_inches='tight')
    return segs, points

# -----------------------------------------------------------------------------------

#input_sizes = [10, 100, 1000 , 10000, 100000, 1000000]
# input_sizes = [10, 100,1000]
#input_sizes = np.arange(10,100101,1000)
# input_sizes = np.arange(10,20101,200) # 101,upto 20000
input_sizes = np.arange(10,100101,3000) #34, up to 100000


average_times = []
runs_per_size = 100

for i in input_sizes:
    print(i)
    average = 0
    for a in tqdm(range(runs_per_size)):
        S, P = GenerateSegments2(i, 10000, 0)
        start = time.time()
        Any_Segments_Intersect(S,P)
        end = time.time()
        
        run_time = end - start
        average += run_time
    average_times.append(average/runs_per_size)

    
# save time
timestr = time.strftime("%Y-%m-%d__%H_%M_%S")
np.savetxt(str(timestr),average_times)
